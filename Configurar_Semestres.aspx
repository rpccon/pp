﻿<%@ Page Title="Configurar Semestres" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Configurar_Semestres.aspx.cs" Inherits="SICRER_Web_Application.Configurar_Semestres" EnableEventValidation="true"%>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Content/Site.css" rel="stylesheet" />
    <link href="Content/jquery-ui-smoothness-1.11.4.min.css" rel="stylesheet" />
    <link href="Content/jquery-ui-smoothness-theme-1.11.4.css" rel="stylesheet" />
    <script src="Scripts/jquery-ui-1.11.4.js"></script>
    <script src="Scripts/jquery.ui.datepicker-es.js"></script>
    <script src="Scripts/site.js"></script>

    <div class="col-md-3 space-up">
        <ul class="nav nav-pills nav-stacked well">
            <li><a href="ConfigurarAdministradores.aspx">Usuarios</a></li>
            <li class="active"><a href="Configurar_Semestres.aspx">Semestres</a></li>
            <li><a href="Indicadores.aspx">Indicadores</a></li>
            <li><a href="Carreras.aspx">Carreras</a></li>
             <li id="confEmail" runat="server" visible="false"><a href="Configurar_Correo.aspx">Correo</a></li>
        </ul>
    </div>

    <div class="col-md-9 space-up">
        
        <asp:Panel ID="panelMsjExito" runat="server" CssClass="exito" Visible="false">
            <asp:Image ID="imgInd" runat="server" ImageUrl="~/img/ok.png"/>
            <asp:Label ID="lbInd" runat="server" Text="" Font-Bold="true"></asp:Label>
        </asp:Panel>

        <asp:Panel ID="panelMsjError" runat="server" CssClass="error" Visible="false">
            <asp:Image ID="imgIndError" runat="server" ImageUrl="~/img/error_icon.png"/>
            <asp:Label ID="lbIndError" runat="server" Text="" Font-Bold="true"></asp:Label>
        </asp:Panel>

        <asp:Panel ID="panelMsjAdv" runat="server" CssClass="alerta" Visible="false">
            <asp:Image ID="imgIndAdv" runat="server" ImageUrl="~/img/warning_icon.png"/>
            <asp:Label ID="lbIndAdv" runat="server" Text="" Font-Bold="true"></asp:Label>
        </asp:Panel>

        <div class="form-inline">
            <asp:LinkButton ID="linkNuevo" runat="server" Text=" Crear Nuevo " OnClientClick="showFormRegSem(); return false;" CssClass="btn btn-primary" />
        </div>

        <div id="formRegistrarSem" class="space-up" style="display: none;">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Registrar Semestre</div>
                </div>
                <div class="panel-body form-horizontal">
                    <div class="form-group">
                        <asp:Label ID="Label1" runat="server" Text="Fecha de inicio " CssClass="control-label col-sm-3" Font-Bold="true"></asp:Label>
                        <asp:TextBox ID="tbDate3" runat="server" CssClass="form-control col-sm-4" Width="110">
                        </asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label2" runat="server" Text="Fecha final " CssClass="control-label col-sm-3" Font-Bold="true"></asp:Label>
                        <asp:TextBox ID="tbDate4" runat="server" CssClass="form-control col-sm-4" Width="110">
                        </asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lbPeriodo" runat="server" Text="Período " CssClass="control-label col-sm-3" Font-Bold="true"></asp:Label>
                        <asp:DropDownList ID="ddlPeriodo" runat="server" CssClass="form-control col-sm-4" Width="80">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lbActual" runat="server" Text="¿Desea que este sea el semestre en uso de ahora en adelante? "
                            CssClass="control-label col-sm-3" Font-Bold="true"></asp:Label>
                        <asp:DropDownList ID="ddlActual" runat="server" CssClass="form-control col-sm-4" Width="80">
                            <asp:ListItem Value="1">Sí</asp:ListItem>
                            <asp:ListItem Value="2">No</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-8" style="align-content: center">
                            <asp:Button ID="btnRegistrarSem" OnClick="btnRegistrarSem_Click" runat="server" Text="Agregar" CssClass="btn btn-success" />
                            <asp:Button ID="btnCancelarRegistroSem" runat="server" Text="Cancelar" CssClass="btn btn-primary" OnClientClick="hideFormRegSem() ; return false;" />
                        </div>
                    </div>
                    <div class="space-up">
                        <asp:Label ID="lbNota" runat="server" Text="Nota: " CssClass="control-label" Font-Bold="true"></asp:Label>
                        <asp:Label ID="LbMsjNota" runat="server" Text="La sincronización de datos se hará según el semestre actual de la tabla de abajo, asegúrese de que sea ese el semestre que desea hacer la copia de los estudiantes." CssClass="control-label"></asp:Label>
                    </div>
                </div>
            </div>
        </div>

        <br />
        <asp:GridView ID="gvSemestres" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered"
            DataKeyNames="ID_SEMESTRE"
            OnRowEditing="gvSemestres_RowEditing"
            OnRowUpdating="gvSemestres_RowUpdating"
            OnRowCancelingEdit="gvSemestres_RowCancelingEdit">
            <Columns>
                <asp:TemplateField HeaderText="ACTUAL">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkSeleccion" runat="server" HeaderText="ACTUAL" Enabled="true"
                            Checked='<%#Convert.ToBoolean(Eval("ACTUAL"))%>'
                            OnCheckedChanged="chkSeleccion_CheckedChanged" AutoPostBack="true" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PERIODO">
                    <ItemTemplate>
                        <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control col-sm-4" 
                             OnDataBinding="DropDownList1_DataBinding" Width="80" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="INICIO">
                    <ItemTemplate>
                        <asp:TextBox ID="tbDate1" runat="server" ClientIDMode="AutoID"
                            Text='<%# Eval("INICIO") %>' Enabled="false" Width="120" CssClass="form-control col-sm-4">
                        </asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="FINAL">
                    <ItemTemplate>
                        <asp:TextBox ID="tbDate2" runat="server" ClientIDMode="AutoID"
                            Text='<%# Eval("FINAL") %>' Enabled="false" Width="120" CssClass="form-control col-sm-4">
                        </asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:CommandField ShowEditButton="True" ButtonType="Button" ControlStyle-CssClass="btn btn-warning">
                    <ControlStyle CssClass="btn btn-warning"></ControlStyle>
                </asp:CommandField>
            </Columns>
        </asp:GridView>
        <script src="Scripts/bootstrap.min.js"></script>
    </div>
</asp:Content>
