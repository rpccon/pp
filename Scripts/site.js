﻿1/***********************************************************
Archivo js para escribir funciones personalizadas para los
diferentes usos de la pagina.

Autor: Luis Martínez
Fecha de Creación: 25-02-15
************************************************************/

function hideElement()
{
    document.getElementById("gvRegistrados").style.display = "none";
}

function showElement()
{
    document.getElementById("gvRegistrados").style.display = "block";
}

function showFormRegSem() {
    document.getElementById("formRegistrarSem").style.display = "block";
}

function hideFormRegSem() {
    document.getElementById("formRegistrarSem").style.display = "none";

}

function hideFormRegAdmi() {
    document.getElementById("formRegistrarAdmi").style.display = "none";
}

function showFormRegAdmi() {
    //document.getElementById("formRegistrarAdmi").style.display = "block";
    administradores();
}

function ocultarFormulario() {

    $("#formulario").css("display", "none");
}
function mostrarFormulario() {

    $("#formulario").css("display", "block");

}

function mostrarNotificacion(elemento) {
    $(elemento).fadeIn(1000);
    $(elemento).fadeOut(30000);

}

function ocultarNotificacionN(elemento) {
    $(elemento).fadeOut(10000);
}

function selectorRevisor() {
    $("#tipoUsuario").text("Registro de Revisor");
    $("#divTelefono").css("display", "none");
    $("#divEmail").css("display", "none");

    $("#tipoRev").addClass("active");
    $("#tipoAdmin").removeClass("active");
    $("#tipoCor").removeClass("active");
    $("#tbNombre").attr("disabled", false);
    $("#buscar").attr("placeholder", "Buscar Revisor");
}
function selectorAdministrador() {
    $("#tipoUsuario").text("Registro de Administrador");
    $("#divTelefono").css("display", "block");
    $("#divEmail").css("display", "block");
    $("#tbNombre").attr("disabled", false);
    $("#tipoRev").removeClass("active");
    $("#tipoAdmin").addClass("active");
    $("#tipoCor").removeClass("active");
    $("#lblEmail").text("Email");
    $("#tbEmail").attr("type", "email");
    $("#tbEmail").attr("placeholder", "Email");
    $("#buscar").attr("placeholder", "Buscar Administrador");
}
function selectorCoordinador() {
    $("#tipoUsuario").text("Registro de Coordinador");
    $("#divTelefono").css("display", "none");
    $("#tbNombre").attr("disabled", true);
    $("#tbNombre").attr("placeholder", "Estudiante");
    $("#tipoRev").removeClass("active");
    $("#tipoAdmin").removeClass("active");
    $("#divEmail").css("display", "block");
    $("#divTelefono").css("display", "none");
    $("#lblEmail").text("Carné");
    $("#tbEmail").attr("type", "number");
    $("#tbEmail").attr("placeholder", "Carné");
    $("#tipoCor").addClass("active");
    $("#buscar").attr("placeholder", "Buscar Coordinador");
}


$(document).ready(function () {////dfsgfdgfdgdfdgdfgdfgfdgdf
    $("[id$=tbDate1]").datepicker();

    $("[id$=tbDate2]").datepicker();

    $("#MainContent_tbDate3").datepicker();
    $("#MainContent_PDFA").datepicker();

    $("#MainContent_PDFB").datepicker();
    $("[id$=NP_inicial]").datepicker();
    $("[id$=NP_final]").datepicker();

    $("#MainContent_tbDate4").datepicker();

    $("#MainContent_txtSelFechaInicio").datepicker();

    $("#MainContent_txtSelFechaFinal").datepicker();

   

})

var config = {
    ipAdress:"172.24.42.52"
}