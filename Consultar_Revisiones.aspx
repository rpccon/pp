﻿<%@ Page Title="Consultar Revisiones" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Consultar_Revisiones.aspx.cs" 
    Inherits="SICRER_Web_Application.Consultar_Revisiones" EnableEventValidation="true"%>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Content/jquery-ui-smoothness-1.11.4.min.css" rel="stylesheet" />
    <link href="Content/jquery-ui-smoothness-theme-1.11.4.css" rel="stylesheet" />
    <link href="Content/revisiones.css" rel="stylesheet" />
    <script src="Scripts/jquery-ui-1.11.4.js"></script>
    <script src="Scripts/jquery.ui.datepicker-es.js"></script>
    <script src="Scripts/revisiones.js"></script>
    <script src="Scripts/site.js"></script>

    <style>
        @media only screen and (max-width: 768px) {
             .form-inline {
                display: block;
                margin-top: 10px;
                margin-bottom:10px;
            }
            .btn-primary btn-default{
                margin-top: 10px;
                margin-bottom:5px;
            }
            .form-control{
                margin-bottom:5px;
            }
        }
    </style>

    <div class="col-md-12">

        <div class="col-md-3 space-up">
            <ul class="nav nav-pills nav-stacked well">
                <li class="active"><a href="Consultar_Revisiones.aspx"> Consultar Revisiones</a></li>
                <li><a href="Configurar_Grupos_Admin.aspx"> Consultar Grupos</a></li>
                <li><a href="Consultar_Estudiantes.aspx">Consultar Estudiantes</a></li>
                <li><a href="Consultar_Activos_Inactivos.aspx">Consultar Activos/Inactivos</a></li>
            </ul>
        </div>

        <div class="col-md-9 space-up">
            <asp:Panel ID="panelMsjExito" runat="server" CssClass="exito" Visible="false">
                <asp:Image ID="imgInd" runat="server" ImageUrl="~/img/ok.png"/>
                <asp:Label ID="lbInd" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>

            <asp:Panel ID="panelMsjError" runat="server" CssClass="error" Visible="false">
                <asp:Image ID="imgIndError" runat="server" ImageUrl="~/img/error_icon.png"/>
                <asp:Label ID="lbIndError" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>

            <asp:Panel ID="panelMsjAdv" runat="server" CssClass="alerta" Visible="false">
                <asp:Image ID="imgIndAdv" runat="server" ImageUrl="~/img/warning_icon.png"/>
                <asp:Label ID="lbIndAdv" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>

            <div id="consulta_rev_panel" class="form-inline">
                <asp:DropDownList id="ddTp_ind" runat="server" CssClass="selectpicker form-control" Width="130px" AutoPostBack="false">
                    <asp:ListItem Text="Cocina" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Cuarto" Value="2"></asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList id="ddResidencias" runat="server" DataTextField="Residencia" CssClass="selectpicker form-control" Width="130px" AutoPostBack="false">
                    <asp:ListItem Text="Residencias" Value="0"></asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList id="ddFiltros" runat="server" CssClass="selectpicker form-control" Width="130px" AutoPostBack="false">
                    <asp:ListItem Text="Mostrar Todo" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Ala A" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Ala B" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Llamadas Atención" Value="3"></asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox id="txtSelFechaInicio" runat="server" CssClass="form-control" Width="130px"/>
                <asp:TextBox id="txtSelFechaFinal" runat="server" CssClass="form-control" Width="130px"/>
                <asp:LinkButton id="linkConsultar" runat="server" Text="<span class='glyphicon glyphicon-search'></span> Consultar" CssClass="btn btn-primary" OnClick="linkConsultar_Click"/>

            </div>
            <div class="form-inline">

            </div>
               
            <div class="form-group space-up">
                <asp:Label ID="lbMsjNoRevision" runat="server" Text="" ForeColor="Black" Font-Bold="true" Font-Italic="true" Font-Size="Medium" Visible="false"></asp:Label>
            </div>

            <div id="prueba_xx">
            <!--Gridview que muestra las revisiones de todas las residencias-->
            <div class="table-responsive" id="gvResponsive" runat="server" visible="false">
                <asp:GridView id="gvRevisiones1" 
                    runat="server" 
                    AutoGenerateColumns="False" 
                    CssClass="table table-bordered table-hover space-up" 
                    OnRowCommand="gvRevisiones1_RowCommand">

                    <Columns>
                        <asp:TemplateField HeaderText="Select">
                            <HeaderTemplate>
                                <asp:CheckBox id="chkAll" runat="server" ClientIDMode="Static" onclick="Selectallcheckbox(this);"/>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" ClientIDMode="Static"/>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField  DataField="FECHA_REV" HeaderText="Fecha Revisión"/>
                        <asp:BoundField  DataField="ESTADO" HeaderText="Calificación"/>
                        <asp:BoundField  DataField="RESIDENCIA" HeaderText="Residencia"/>
                        <asp:BoundField  DataField="ALA_CUARTO" HeaderText="Ala / Cuarto"/>
                        <asp:BoundField  DataField="LLAMADA_ATENCION_RES" HeaderText="Llamada Atención" />
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnVerEst" runat="server" 
                                    CommandName="VerEst" 
                                    CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                                    Text="<span class='glyphicon glyphicon-info-sign'></span> Detalles "
                                    CssClass="btn btn-info"
                                    ClientIDMode="AutoID"
                                    />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                                <asp:LinkButton ID="btnEditar" runat="server" 
                                    CommandName="Editar" 
                                    CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                                    Text="<span class='glyphicon glyphicon-edit'></span> Editar "
                                    CssClass="btn btn-warning"
                                    />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <!--Gridview que muestra las revisiones para la residencia seleccionada-->
                <asp:GridView id="gvRevisiones2" 
                    runat="server" 
                    AutoGenerateColumns="False" 
                    CssClass="table table-bordered table-hover space-up" 
                    OnRowCommand="gvRevisiones1_RowCommand">

                    <Columns>
                        <asp:TemplateField HeaderText="Select">
                            <HeaderTemplate>
                                <asp:CheckBox id="chkAll" runat="server" ClientIDMode="Static" onclick="Selectallcheckbox(this);"/>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" ClientIDMode="Static"/>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField   DataField="FECHA_REV" HeaderText="Fecha Revisión"/>
                        <asp:BoundField  DataField="ESTADO" HeaderText="Calificación"/>
                        <asp:BoundField  DataField="ALA_CUARTO" HeaderText="Ala o Cuarto"/>
                        <asp:BoundField  DataField="LLAMADA_ATENCION_RES" HeaderText="Llamada Atención" />
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnVerEst" runat="server" 
                                    CommandName="VerEst" 
                                    CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                                    Text="<span class='glyphicon glyphicon-info-sign'></span> Detalles "
                                    CssClass="btn btn-info"
                                    ClientIDMode="AutoID"
                                    />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                                <asp:LinkButton ID="btnEditar" runat="server" 
                                    CommandName="Editar" 
                                    CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                                    Text="<span class='glyphicon glyphicon-edit'></span> Editar "
                                    CssClass="btn btn-warning"
                                    />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField  ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnNotificar" runat="server" 
                                    CommandName="Notificar" 
                                    CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                                    Text="<span class='glyphicon glyphicon-envelope'></span> Notificar estudiantes "
                                    CssClass="btn btn-warning"
                                    />

                                    <asp:LinkButton ID="btnPrenotificar" runat="server" visible="false"
                                    CommandName="Prenotificar" 
                                    CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                                    Text="<span class='glyphicon glyphicon-envelope'></span> Pre-Notificar estudiantes"
                                    CssClass="btn btn-info"
                                    />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
         </div>


            </div>


            <!-- Cuadro para escribir texto de prenotificar  <-->

            <div id="panelPreN" class="panel panel-default" style="display:none;">
                 <div id="div_title_pren" class="panel-heading">
                        <div class="panel-title">Pre-notificar estudiantes</div>
                </div>
                <div id="panel_setDesalojo" align="center" class="panel-body form-horizontal">
                   <div id="subpanel_pren" class="form-inline space-up space-down">
                        <asp:Label ID="lb_askB" Width="120" runat="server" >Indique la nota</asp:Label>
                        <asp:TextBox ID="pre_nota" runat="server"  Height="80px" Width="300px" TextMode="MultiLine"></asp:TextBox>
                    </div>

                    <div  class="form-inline space-up space-down">
                        <asp:LinkButton ID="btn_preN" CssClass="btn btn-warning" OnClick="sendNote"  runat="server">Continuar</asp:LinkButton> <!--OnClick="continuar_click"   OnSelectedIndexChanged="casos_cocina" -->
                    </div>

                </div>

            </div>


            <!-- -->
         <!-- Modal -->
        <div id="modalEst" class="modal fade" role="dialog">
          <div class="modal-dialog  ">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 id="tituloDetalle" runat="server" class="modal-title">Detalle Revisión</h4>
              </div>
              <div class="modal-body">
                <div style ="margin-bottom:10px; margin-top: -5px">
                    <strong id="subDetalle" runat="server"></strong>
                </div>
                <asp:LinkButton ID="btnMostrarEst" runat="server" CssClass="btn btn-info" Text="<span class='glyphicon glyphicon-chevron-right'></span> Estudiantes"  OnClientClick="mostrarEst(); return false;"></asp:LinkButton>
                <asp:LinkButton ID="btnMostrarObs" runat="server" CssClass="btn btn-info" Text="<span class='glyphicon glyphicon-chevron-right'></span> Observaciones"  OnClientClick="mostrarObs(); return false;"></asp:LinkButton>

                <div id="capaEstudiante" style="display:none" class="space-up">
                     <asp:GridView ID="gvEstudiantes" runat="server" 
                        AutoGenerateColumns="false"
                        CssClass="table table-bordered table-hover" 
                        >
                        <Columns>
                            <asp:BoundField DataField="CARNE" HeaderText="Carné"/>
                            <asp:BoundField DataField="NOMBRE" HeaderText="Nombre"/>
                        </Columns>
                    </asp:GridView>
                 </div>
                 <div id="capaObservaciones"  style="display:none" class="space-up">
                     <p id="obsEst" runat="server" style="text-justify:auto"></p>
                 </div>
               </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
            </div>
          </div>

            <asp:LinkButton 
                id="LinkCrearPDF" 
                runat="server" 
                Text="<span class='glyphicon glyphicon-save file'></span> Crear Archivo Pdf" 
                CssClass="btn btn-default  space-down-footer" 
                Visible="false"
                CommandName=""
                OnCommand="LinkCrearPDF_Command" 
                OnClientClick="ocultarElemento('MainContent_panelMsjError');"/>

             <asp:Panel id="panelCargardoPDF" class="hide-ele" runat="server">
                <asp:Image ID='imgCargando1' runat='server' ImageUrl='~/img/load_transaction.gif' />
                <asp:Label ID="lbPdf2" runat="server" Text="Creando PDF ..." Font-Bold="true" Font-Size="Small"></asp:Label>
            </asp:Panel>

        </div>
        <script src="Scripts/bootstrap.min.js"></script>
   </div>
</asp:Content>
